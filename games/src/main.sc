require: number/number.sc
    module = zenbot-common
    
patterns:
$hello = (здравствуй*|привет|добрый (день|вечер)| доброе утро|доброй ночи)
$yes = (ага|ок|окей|да|давай|хорошо|ладно|хочу)
$no = (нет|не (хочу|буду|сейчас)|неа)
$match = (ага|да|угадал*|правильно|верно|равно)
$Number = $regesp<\d+> || converter = numberConverterDigit

theme: /numbers

state: Hello
    q!: $hello
    a: Здравствуйте!
    go!: /Would you like to play?

  state: Would you like to play?
    a: Хотите сыграть в игру "Угадай число"?

    state: No
      q: $no
      a: Как-нибудь в другой раз! Всего хорошего.

    state: Yes
      q: $yes
      a: Отлично! Выберите, кто загадывает число - вы или я?

      state: user
        q: {[думаю|наверно*|пожалуй|пусть|давай] (я)}
        a: Отлично! Пожалуйста, напишите, когда загадаете число от 1 до 100. 
        script:
          $session.Left = 1;
          $session.Right = 100;
          $session.GuessNumber = ($session.Left + $session.Right)/2;
          go!: /Ready

          state: Ready
            q!: (готов*|загадал*|вс(е|ё)|ага|ок|окей|да)
            a: Хорошо. Мое предположение - {{ $session.GuessNumber }}. Напишите, пожалуйста, загаданное Вами число больше, меньше или равно ему?

            state: Bigger
              q: больше
              script:
                $session.Left = $session.GuessNumber;
                $session.GuessNumber = ($session.Left + $session.Right)/2;
              go!: /Ready

            state: Smaller 
              q: меньше 
              script:
                $session.Right = $session.GuessNumber;
                $session.GuessNumber = ($session.Left + $session.Right)/2;
              go!: /Ready 

            state: Match
              q: $match
              a: Здорово! Мне понравилось выигрывать ;)
              go!: /Would you like to play?


      state: chat-bot
        q!: {[думаю|наверно*|пожалуй|пусть|давай] (бот|чат*|чат-бот|комп*|вы|ты|программа)}
        script:
          $session.RandomNumber = Math.floor(Math.random() * 100) + 1;  
        a: Хорошо. Я выбрал число, пришло время играть!
        go!: /User guess

        state: User guess
          q: * $Number *
          script:
            $temp.Number = $parseTree._Number;
          if $temp.Number > $session.RandomNumber:
            a: Ваш ответ больше загаданного мной числа.
            go!: /User guess
          if $temp.Number < $session.RandomNumber:
            a: Ваш ответ меньше загаданного мной числа. 
            go!: /User guess 
          else:
            a: Вы угадали! 
            go!: /Would you like to play? 

                            
state: CatchAll || noContext = true
  q!: *
  a: Простите, я вас не понимаю.